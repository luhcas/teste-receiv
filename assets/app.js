$(document).ready( function () {
    $('#myTable').DataTable({
      initComplete: function() {
        $(this.api().table().container()).find('input').parent().wrap('<form>').parent().attr('autocomplete', 'off');
      }
    });
} );


$(document).on('click', '.editDebt', function(){
  $('#idDebit').val($(this).data('id'));
  $('#editTitle').val($(this).data('title'));
  $('#editAmount').val($(this).data('amount'));
  $('#editDue_date').val($(this).data('due_date'));
  $('#editStatus').val($(this).data('status'));

  $('#editDebt').modal('show');
});
