<?php

require_once 'Crud.php';

class Debts extends Crud{

	protected $table = 'debts';
	private $id_debtor;
  private $title;
	private $amount;
	private $due_date;
  private $status;

	public function setIdDebtor($id_debtor){
		$this->id_debtor = $id_debtor;
	}

	public function setTitle($title){
		$this->title = $title;
	}

  public function setAmount($amount){
		$this->amount = $amount;
	}

  public function setDueDate($due_date){
		$this->due_date = $due_date;
	}

	public function setStatus($status){
		$this->status = $status;
	}

	public function insert(){
		$sql  = "INSERT INTO $this->table (id_debtor, title, amount, due_date) VALUES (:id_debtor, :title, :amount, :due_date)";
		$stmt = DB::prepare($sql);
		$stmt->bindParam(':id_debtor', $this->id_debtor);
    $stmt->bindParam(':title', $this->title);
    $stmt->bindParam(':amount', $this->amount);
    $stmt->bindParam(':due_date', $this->due_date);
		return $stmt->execute();
	}

	public function update($id){
		$sql  = "UPDATE $this->table SET title = :title, amount = :amount, due_date = :due_date, status = :status WHERE id = :id";
		$stmt = DB::prepare($sql);
		$stmt->bindParam(':title', $this->title);
    $stmt->bindParam(':amount', $this->amount);
		$stmt->bindParam(':due_date', $this->due_date);
    $stmt->bindParam(':status', $this->status);
		$stmt->bindParam(':id', $id);
		return $stmt->execute();
	}

	public function findDebtsDebtors($id){
		$sql  = "SELECT * FROM $this->table WHERE id_debtor = :id";
		$stmt = DB::prepare($sql);
		$stmt->bindParam(':id', $id, PDO::PARAM_INT);
		$stmt->execute();
		return $stmt->fetchAll();
	}

	public function findPayed(){
		$sql  = "SELECT count(id) as quantity, sum(amount) as amount FROM $this->table WHERE status = '1'";
		$stmt = DB::prepare($sql);
		$stmt->execute();
		return $stmt->fetch();
	}

	public function findNotPayed(){
		$sql  = "SELECT count(id) as quantity, sum(amount) as amount FROM $this->table WHERE status = '0'";
		$stmt = DB::prepare($sql);
		$stmt->execute();
		return $stmt->fetch();
	}

	public function findAllReportDebit(){
		$sql  = "SELECT count(id) as quantity, sum(amount) as amount FROM $this->table";
		$stmt = DB::prepare($sql);
		$stmt->execute();
		return $stmt->fetch();
	}

	public function findLastPayments(){
		$sql  = "SELECT * FROM $this->table  WHERE status = '1' ORDER BY updated_at DESC limit 10";
		$stmt = DB::prepare($sql);
		$stmt->execute();
		return $stmt->fetchAll();
	}

}
