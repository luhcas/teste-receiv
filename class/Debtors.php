<?php

require_once 'Crud.php';

class Debtors extends Crud{

	protected $table = 'debtors';
	private $name;
  private $identity;
  private $birth;
  private $address;
  private $number;
  private $neighborhood;
  private $city;
  private $uf;
	private $zipcode;

	public function setName($name){
		$this->name = $name;
	}

	public function getName(){
		return $this->name;
	}

	public function setIdentity($identity){
		$this->identity = $identity;
	}

  public function setBirth($birth){
		$this->birth = $birth;
	}

  public function setAddress($address){
		$this->address = $address;
	}

  public function setNumber($number){
		$this->number = $number;
	}

  public function setNeighborhood($neighborhood){
		$this->neighborhood = $neighborhood;
	}

  public function setCity($city){
		$this->city = $city;
	}

  public function setUf($uf){
		$this->uf = $uf;
	}

  public function setZipcode($zipcode){
		$this->zipcode = $zipcode;
	}

	public function insert(){
		$sql  = "INSERT INTO $this->table (name, identity, birth, address, number, neighborhood, city, uf, zipcode) VALUES (:name, :identity, :birth, :address, :number, :neighborhood, :city, :uf, :zipcode)";
		$stmt = DB::prepare($sql);
		$stmt->bindParam(':name', $this->name);
    $stmt->bindParam(':identity', $this->identity);
    $stmt->bindParam(':birth', $this->birth);
    $stmt->bindParam(':address', $this->address);
    $stmt->bindParam(':number', $this->number);
    $stmt->bindParam(':neighborhood', $this->neighborhood);
    $stmt->bindParam(':city', $this->city);
    $stmt->bindParam(':uf', $this->uf);
		$stmt->bindParam(':zipcode', $this->zipcode);
		return $stmt->execute();
	}

	public function update($id){
		$sql  = "UPDATE $this->table SET name = :name, identity = :identity, birth = :birth, address = :address, number = :number, neighborhood = :neighborhood, city = :city, uf = :uf, zipcode = :zipcode WHERE id = :id";
		$stmt = DB::prepare($sql);
		$stmt->bindParam(':name', $this->name);
    $stmt->bindParam(':identity', $this->identity);
    $stmt->bindParam(':birth', $this->birth);
    $stmt->bindParam(':address', $this->address);
    $stmt->bindParam(':number', $this->number);
    $stmt->bindParam(':neighborhood', $this->neighborhood);
    $stmt->bindParam(':city', $this->city);
    $stmt->bindParam(':uf', $this->uf);
		$stmt->bindParam(':zipcode', $this->zipcode);
		$stmt->bindParam(':id', $id);
		return $stmt->execute();
	}

}
