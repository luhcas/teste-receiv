<?php require_once('elements/header.php'); ?>
<div class="container">
  <?php if (isset($_SESSION['flash'])): ?>
    <div class="alert alert-primary mt-3" role="alert">
      <?php echo $_SESSION['flash']; ?>
    </div>
    <?php unset($_SESSION['flash']); ?>
  <?php endif; ?>
  <h1 class="text-center text-danger mt-3">Devedores</h1>
  <a href="create-debtor.php" class="btn btn-primary mb-3">Cadastrar devedor</a>

  <table id="myTable" class="display">
    <thead>
      <tr>
        <th>Nome</th>
        <th>Identidade</th>
        <th>Endereço</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <?php $debtor = new Debtors(); ?>
      <?php foreach ($debtor->findAll() as $key => $value): ?>
        <tr>
          <td><?php echo $value->name; ?></td>
          <td><?php echo $value->identity; ?></td>
          <td>
            <?php echo $value->address; ?>,
            <?php echo $value->number; ?>,
            <?php echo $value->neighborhood; ?>,
            <?php echo $value->city; ?> -
            <?php echo $value->uf; ?>
          </td>
          <td>
            <a href="debtor.php?id=<?php echo $value->id; ?>">Ver</a>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
</div>
<?php require_once('elements/footer.php'); ?>
