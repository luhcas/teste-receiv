<?php require_once('elements/header.php'); ?>
<div class="container">
  <?php if (isset($_SESSION['flash'])): ?>
    <div class="alert alert-primary mt-3" role="alert">
      <?php echo $_SESSION['flash']; ?>
    </div>
    <?php unset($_SESSION['flash']); ?>
  <?php endif; ?>
  <h1 class="text-center text-danger mt-3">Devedor</h1>
  <?php
    $debtor = new Debtors();
    $id = (int)$_GET['id'];
    $return = $debtor->find($id);
  ?>

  <form class="" action="updateDebtor.php" method="post">
    <div class="form-group">
      <label for="name">Nome</label>
      <input type="text" class="form-control" id="name" name="name" value="<?php echo $return->name; ?>" required>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label for="identity">CPF / CNPJ</label>
          <input type="text" class="form-control" id="identity" name="identity" value="<?php echo $return->identity; ?>" required>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label for="birth">Data de nascimento</label>
          <input type="date" class="form-control" id="birth" name="birth" value="<?php echo $return->birth; ?>" required>
        </div>
      </div>
    </div>
    <div class="form-group">
      <label for="address">Endereço</label>
      <input type="text" class="form-control" id="address" name="address" value="<?php echo $return->address; ?>" required>
    </div>
    <div class="row">
      <div class="col-md-4">
        <div class="form-group">
          <label for="number">Número</label>
          <input type="text" class="form-control" id="number" name="number" value="<?php echo $return->number; ?>" required>
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label for="neighborhood">Bairro</label>
          <input type="text" class="form-control" id="neighborhood" name="neighborhood" value="<?php echo $return->neighborhood; ?>" required>
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label for="city">Cidade</label>
          <input type="text" class="form-control" id="city" name="city" value="<?php echo $return->city; ?>" required>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label for="uf">Estado</label>
          <select required id="uf" name="uf" class="form-control" required>
            <option value="">Selecione</option>
            <option value="AC" <?php echo ($return->uf == "AC")? "selected" : ""; ?>>Acre</option>
            <option value="AL" <?php echo ($return->uf == "AL")? "selected" : ""; ?>>Alagoas</option>
            <option value="AP" <?php echo ($return->uf == "AP")? "selected" : ""; ?>>Amapá</option>
            <option value="AM" <?php echo ($return->uf == "AM")? "selected" : ""; ?>>Amazonas</option>
            <option value="BA" <?php echo ($return->uf == "BA")? "selected" : ""; ?>>Bahia</option>
            <option value="CE" <?php echo ($return->uf == "CE")? "selected" : ""; ?>>Ceará</option>
            <option value="DF" <?php echo ($return->uf == "DF")? "selected" : ""; ?>>Distrito Federal</option>
            <option value="ES" <?php echo ($return->uf == "ES")? "selected" : ""; ?>>Espírito Santo</option>
            <option value="GO" <?php echo ($return->uf == "GO")? "selected" : ""; ?>>Goiás</option>
            <option value="MA" <?php echo ($return->uf == "MA")? "selected" : ""; ?>>Maranhão</option>
            <option value="MT" <?php echo ($return->uf == "MT")? "selected" : ""; ?>>Mato Grosso</option>
            <option value="MS" <?php echo ($return->uf == "MS")? "selected" : ""; ?>>Mato Grosso do Sul</option>
            <option value="MG" <?php echo ($return->uf == "MG")? "selected" : ""; ?>>Minas Gerais</option>
            <option value="PA" <?php echo ($return->uf == "PA")? "selected" : ""; ?>>Pará</option>
            <option value="PB" <?php echo ($return->uf == "PB")? "selected" : ""; ?>>Paraíba</option>
            <option value="PR" <?php echo ($return->uf == "PR")? "selected" : ""; ?>>Paraná</option>
            <option value="PE" <?php echo ($return->uf == "PE")? "selected" : ""; ?>>Pernambuco</option>
            <option value="PI" <?php echo ($return->uf == "PI")? "selected" : ""; ?>>Piauí</option>
            <option value="RJ" <?php echo ($return->uf == "RJ")? "selected" : ""; ?>>Rio de Janeiro</option>
            <option value="RN" <?php echo ($return->uf == "RN")? "selected" : ""; ?>>Rio Grande do Norte</option>
            <option value="RS" <?php echo ($return->uf == "RS")? "selected" : ""; ?>>Rio Grande do Sul</option>
            <option value="RO" <?php echo ($return->uf == "RO")? "selected" : ""; ?>>Rondônia</option>
            <option value="RR" <?php echo ($return->uf == "RR")? "selected" : ""; ?>>Roraima</option>
            <option value="SC" <?php echo ($return->uf == "SC")? "selected" : ""; ?>>Santa Catarina</option>
            <option value="SP" <?php echo ($return->uf == "SP")? "selected" : ""; ?>>São Paulo</option>
            <option value="SE" <?php echo ($return->uf == "SE")? "selected" : ""; ?>>Sergipe</option>
            <option value="TO" <?php echo ($return->uf == "TO")? "selected" : ""; ?>>Tocantins</option>
          </select>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label for="zipcode">CEP</label>
          <input type="text" class="form-control" id="zipcode" name="zipcode" value="<?php echo $return->zipcode; ?>" required>
        </div>
      </div>
    </div>
    <div class="form-group">
      <input type="hidden" name="id" value="<?php echo $id; ?>" required>
      <input type="submit" class="btn btn-primary float-right" name="update" value="Atualizar">
    </div>
  </form>
  <form class="" action="destroyDebtor.php" method="post">
    <input type="hidden" name="id" value="<?php echo $id; ?>" required>
    <input type="submit" class="btn btn-danger float-left" name="destroy" value="Deletar">
  </form>

  <div class="row mt-5">
    <div class="col-md-12">
      <h1 class="text-center">Dívidas</h1>
    </div>
  </div>
  <button type="button" class="btn btn-primary mb-3 float-right" data-toggle="modal" data-target="#registerDebt">
    Inserir dívida
  </button>
  <table id="myTable" class="display">
    <thead>
      <tr>
        <th>Título</th>
        <th>Valor</th>
        <th>Vencimento</th>
        <th>Status</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <?php $debts = new Debts(); ?>
      <?php foreach ($debts->findDebtsDebtors($return->id) as $key => $value): ?>
        <tr>
          <td><?php echo $value->title; ?></td>
          <td><?php echo $value->amount; ?></td>
          <td><?php echo $value->due_date; ?></td>
          <td><?php echo ($value->status == 0)? 'Não pago' : 'Pago'; ?></td>
          <td>
            <button type="button" class="btn btn-link editDebt"
                data-id="<?php echo $value->id; ?>"
                data-title="<?php echo $value->title; ?>"
                data-amount="<?php echo $value->amount; ?>"
                data-due_date="<?php echo $value->due_date; ?>"
                data-status="<?php echo $value->status; ?>"
            >Ver</button>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
</div>

<!-- Modal -->
<div class="modal fade" id="registerDebt" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <form class="" action="registerDebt.php" method="post">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Cadastrar divida</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="title">Título</label>
            <input type="text" class="form-control" id="title" name="title" aria-describedby="emailHelp">
          </div>
          <div class="form-group">
            <label for="amount">Valor (R$)</label>
            <input type="text" class="form-control" id="amount" name="amount">
          </div>
          <div class="form-group">
            <label for="due_date">Vencimento</label>
            <input type="date" class="form-control" id="due_date" name="due_date">
          </div>
        </div>
        <div class="modal-footer">
          <input type="hidden" name="id_debtor" value="<?php echo $return->id; ?>">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" name="create" class="btn btn-primary">Inserir</button>
        </div>
      </div>
    </div>
  </form>
</div>

<!-- Modal -->
<div class="modal fade" id="editDebt" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <form class="" action="updateDebt.php" method="post">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Editar divida</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="editTitle">Título</label>
            <input type="text" class="form-control" id="editTitle" name="title" aria-describedby="emailHelp">
          </div>
          <div class="form-group">
            <label for="editAmount">Valor (R$)</label>
            <input type="text" class="form-control" id="editAmount" name="amount">
          </div>
          <div class="form-group">
            <label for="editDue_date">Vencimento</label>
            <input type="date" class="form-control" id="editDue_date" name="due_date">
          </div>
          <div class="form-group">
            <label for="editStatus">Status</label>
            <select class="form-control" name="status" id="editStatus">
              <option value=""></option>
              <option value="0">Não pago</option>
              <option value="1">Pago</option>
            </select>
          </div>
        </div>
        <div class="modal-footer">
          <input type="hidden" name="id" id="idDebit" value="<?php echo $return->id; ?>">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" name="update" class="btn btn-primary">Atualizar</button>
        </div>
      </div>
    </div>
  </form>
</div>
<?php require_once('elements/footer.php'); ?>
