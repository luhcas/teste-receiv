<?php
  function __autoload($class_name){
    require_once './class/' . $class_name . '.php';
  }

	if(isset($_POST['create'])):
    session_start();

    $debtor = new Debts();

		$debtor->setIdDebtor($_POST['id_debtor']);
    $debtor->setTitle($_POST['title']);
    $debtor->setAmount($_POST['amount']);
    $debtor->setDueDate($_POST['due_date']);

		if($debtor->insert()){
			$_SESSION['flash'] = 'Inserido com sucesso!';
		} else {
      $_SESSION['flash'] = 'Houve um problema na tentativa de cadastro!';
    }

    header('Location: debtor.php?id='.$_POST['id_debtor']);
	endif;
?>
