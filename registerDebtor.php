<?php
  function __autoload($class_name){
    require_once './class/' . $class_name . '.php';
  }

	if(isset($_POST['create'])):
    session_start();

    $debtor = new Debtors();

		$debtor->setName($_POST['name']);
    $debtor->setIdentity($_POST['identity']);
    $debtor->setBirth($_POST['birth']);
    $debtor->setAddress($_POST['address']);
    $debtor->setNumber($_POST['number']);
    $debtor->setNeighborhood($_POST['neighborhood']);
    $debtor->setCity($_POST['city']);
    $debtor->setUf($_POST['uf']);
    $debtor->setZipcode($_POST['zipcode']);

		if($debtor->insert()){
			$_SESSION['flash'] = 'Inserido com sucesso!';
		} else {
      $_SESSION['flash'] = 'Houve um problema na tentativa de cadastro!';
    }

    header('Location: index.php');
	endif;
?>
