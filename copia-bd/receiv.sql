-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Tempo de geração: 01-Set-2021 às 11:30
-- Versão do servidor: 8.0.21
-- versão do PHP: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `receiv`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `debtors`
--

DROP TABLE IF EXISTS `debtors`;
CREATE TABLE IF NOT EXISTS `debtors` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb4  NOT NULL,
  `identity` varchar(22) NOT NULL,
  `birth` date NOT NULL,
  `address` varchar(50) CHARACTER SET utf8mb4  NOT NULL,
  `number` varchar(6) NOT NULL,
  `neighborhood` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `uf` varchar(2) NOT NULL,
  `zipcode` varchar(15) NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 ;

--
-- Extraindo dados da tabela `debtors`
--

INSERT INTO `debtors` (`id`, `name`, `identity`, `birth`, `address`, `number`, `neighborhood`, `city`, `uf`, `zipcode`, `created_at`) VALUES
(1, 'Tereza Betina Baptista', '965.845.650-25', '1975-04-01', 'Rua Odete', '256', 'Adriana Parque', 'Anápolis', 'GO', '75053-400', '2021-08-31 14:45:38'),
(2, 'Tiago Cauã Caio Moraes', '337.019.709-04', '2001-02-01', 'Rua Maria Lucena Barbosa', '559', 'Santa Cruz', 'Campina Grande', 'PB', '58417-190', '2021-08-31 15:32:47'),
(3, 'Filipe Paulo Silva', '921.004.007-46', '2002-05-12', 'Rua dos Japonezes', '700', 'Jardim das Américas 3ª Etapa', 'Anápolis', 'GO', '75071-230', '2021-08-31 15:43:50'),
(4, 'Filipe Paulo Silva', '921.004.007-46', '2002-05-12', 'Rua dos Japonezes', '700', 'Jardim das Américas 3ª Etapa', 'Anápolis', 'GO', '75071-230', '2021-08-31 15:43:57'),
(6, 'Fernando Elias Miguel Oliveira', '905.392.441-80', '1995-08-02', 'Rua Itabapoana', '739', 'Itacibá', 'Cariacica', 'ES', '29150-180', '2021-08-31 15:53:51'),
(7, 'Nelson Otávio Cláudio Ferreira', '757.033.116-83', '1977-08-13', 'Rua Associação Atlética Anapolina', '128', 'Polocentro I', 'Anápolis', 'GO', '75130-110', '2021-08-31 15:56:20');

-- --------------------------------------------------------

--
-- Estrutura da tabela `debts`
--

DROP TABLE IF EXISTS `debts`;
CREATE TABLE IF NOT EXISTS `debts` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_debtor` int NOT NULL,
  `title` text NOT NULL,
  `amount` float(10,2) NOT NULL,
  `due_date` date NOT NULL,
  `status` enum('0','1') NOT NULL COMMENT '0 -> não pago, 1 -> pago',
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 ;

--
-- Extraindo dados da tabela `debts`
--

INSERT INTO `debts` (`id`, `id_debtor`, `title`, `amount`, `due_date`, `status`, `created_at`) VALUES
(1, 1, 'Conta de luz', 128.00, '2021-09-18', '1', '2021-08-31 17:53:27'),
(2, 6, 'Conta de gás', 121.00, '2021-09-10', '0', '2021-08-31 18:15:51');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
