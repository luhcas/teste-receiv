<?php
  function __autoload($class_name){
    require_once './class/' . $class_name . '.php';
  }

	if(isset($_POST['destroy'])):
    session_start();

    $debtor = new Debtors();

		if($debtor->delete($_POST['id'])){
			$_SESSION['flash'] = 'Removido com sucesso!';
		} else {
      $_SESSION['flash'] = 'Houve um problema na tentativa de remoção!';
    }

    header('Location: index.php');
	endif;
?>
