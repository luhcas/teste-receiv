<?php require_once('elements/header.php'); ?>
<div class="container">
  <?php if (isset($_SESSION['flash'])): ?>
    <div class="alert alert-primary mt-3" role="alert">
      <?php echo $_SESSION['flash']; ?>
    </div>
    <?php unset($_SESSION['flash']); ?>
  <?php endif; ?>
  <?php $debtor = new Debtors(); ?>
  <?php $debt = new Debts(); ?>

  <h1 class="text-center text-danger mt-3">Relatórios</h1>
  <?php $returnDebtor = $debtor->findCount(); ?>
  <div class="row mb-5">
    <div class="col-md-3">
      <div class="card cardReport">
        <div class="card-body">
          <h5 class="card-title">Devedores</h5>
          <p class="card-text"><?php echo $returnDebtor->quantity; ?></p>
        </div>
      </div>
    </div>
    <?php $payedDebt = $debt->findPayed(); ?>
    <div class="col-md-3">
      <div class="card cardReport">
        <div class="card-body">
          <h5 class="card-title">Pagos</h5>
          <p class="card-text">Qtd: <?php echo $payedDebt->quantity; ?></p>
          <p class="card-text">R$: <?php echo $payedDebt->amount; ?></p>
        </div>
      </div>
    </div>
    <?php $notPayedDebt = $debt->findNotPayed(); ?>
    <div class="col-md-3">
      <div class="card cardReport">
        <div class="card-body">
          <h5 class="card-title">Não pagos</h5>
          <p class="card-text">Qtd: <?php echo $notPayedDebt->quantity; ?></p>
          <p class="card-text">R$: <?php echo $notPayedDebt->amount; ?></p>
        </div>
      </div>
    </div>
    <?php $allReportDebit = $debt->findAllReportDebit(); ?>
    <div class="col-md-3">
      <div class="card cardReport">
        <div class="card-body">
          <h5 class="card-title">Total divida</h5>
          <p class="card-text">Qtd: <?php echo $allReportDebit->quantity; ?></p>
          <p class="card-text">R$: <?php echo $allReportDebit->amount; ?></p>
        </div>
      </div>
    </div>
  </div>
  <h3 class="text-center">Últimos 10 pagamentos</h3>
  <table id="myTable" class="display">
    <thead>
      <tr>
        <th>Título</th>
        <th>Valor</th>
        <th>Vencimento</th>
        <th>Status</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($debt->findLastPayments() as $key => $value): ?>
        <tr>
          <td><?php echo $value->title; ?></td>
          <td><?php echo $value->amount; ?></td>
          <td><?php echo $value->due_date; ?></td>
          <td><?php echo ($value->status == 0)? 'Não pago' : 'Pago'; ?></td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
</div>
<?php require_once('elements/footer.php'); ?>
