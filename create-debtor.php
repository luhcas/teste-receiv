<?php require_once('elements/header.php'); ?>
<div class="container">
  <h1 class="text-center text-danger mt-3">Cadastrar devedor</h1>
  <form class="" action="registerDebtor.php" method="post">
    <div class="form-group">
      <label for="name">Nome</label>
      <input type="text" class="form-control" id="name" name="name" required>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label for="identity">CPF / CNPJ</label>
          <input type="text" class="form-control" id="identity" name="identity" required>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label for="birth">Data de nascimento</label>
          <input type="date" class="form-control" id="birth" name="birth" required>
        </div>
      </div>
    </div>
    <div class="form-group">
      <label for="address">Endereço</label>
      <input type="text" class="form-control" id="address" name="address" required>
    </div>
    <div class="row">
      <div class="col-md-4">
        <div class="form-group">
          <label for="number">Número</label>
          <input type="text" class="form-control" id="number" name="number" required>
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label for="neighborhood">Bairro</label>
          <input type="text" class="form-control" id="neighborhood" name="neighborhood" required>
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label for="city">Cidade</label>
          <input type="text" class="form-control" id="city" name="city" required>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label for="uf">Estado</label>
          <select required id="uf" name="uf" class="form-control" required>
            <option value="">Selecione</option>
            <option value="AC">Acre</option>
            <option value="AL">Alagoas</option>
            <option value="AP">Amapá</option>
            <option value="AM">Amazonas</option>
            <option value="BA">Bahia</option>
            <option value="CE">Ceará</option>
            <option value="DF">Distrito Federal</option>
            <option value="ES">Espírito Santo</option>
            <option value="GO">Goiás</option>
            <option value="MA">Maranhão</option>
            <option value="MT">Mato Grosso</option>
            <option value="MS">Mato Grosso do Sul</option>
            <option value="MG">Minas Gerais</option>
            <option value="PA">Pará</option>
            <option value="PB">Paraíba</option>
            <option value="PR">Paraná</option>
            <option value="PE">Pernambuco</option>
            <option value="PI">Piauí</option>
            <option value="RJ">Rio de Janeiro</option>
            <option value="RN">Rio Grande do Norte</option>
            <option value="RS">Rio Grande do Sul</option>
            <option value="RO">Rondônia</option>
            <option value="RR">Roraima</option>
            <option value="SC">Santa Catarina</option>
            <option value="SP">São Paulo</option>
            <option value="SE">Sergipe</option>
            <option value="TO">Tocantins</option>
          </select>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label for="zipcode">CEP</label>
          <input type="text" class="form-control" id="zipcode" name="zipcode" required>
        </div>
      </div>
    </div>
    <div class="form-group">
      <input type="submit" class="btn btn-primary float-right" name="create">
    </div>
  </form>
</div>
<?php require_once('elements/footer.php'); ?>
