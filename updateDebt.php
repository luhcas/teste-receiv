<?php
  function __autoload($class_name){
    require_once './class/' . $class_name . '.php';
  }

	if(isset($_POST['update'])):
    session_start();

    $debt = new Debts();

    $debt->setTitle($_POST['title']);
    $debt->setAmount($_POST['amount']);
    $debt->setDueDate($_POST['due_date']);
    $debt->setStatus($_POST['status']);

		if($debt->update($_POST['id'])){
			$_SESSION['flash'] = 'Atualizado com sucesso!';
		} else {
      $_SESSION['flash'] = 'Houve um problema na tentativa de edição!';
    }

    header('Location: debtor.php?id='.$_POST['id']);
	endif;
?>
