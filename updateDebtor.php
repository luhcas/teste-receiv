<?php
  function __autoload($class_name){
    require_once './class/' . $class_name . '.php';
  }

	if(isset($_POST['update'])):
    session_start();

    $debtor = new Debtors();

		$debtor->setName($_POST['name']);
    $debtor->setIdentity($_POST['identity']);
    $debtor->setBirth($_POST['birth']);
    $debtor->setAddress($_POST['address']);
    $debtor->setNumber($_POST['number']);
    $debtor->setNeighborhood($_POST['neighborhood']);
    $debtor->setCity($_POST['city']);
    $debtor->setUf($_POST['uf']);
    $debtor->setZipcode($_POST['zipcode']);

		if($debtor->update($_POST['id'])){
			$_SESSION['flash'] = 'Atualizado com sucesso!';
		} else {
      $_SESSION['flash'] = 'Houve um problema na tentativa de edição!';
    }

    header('Location: debtor.php?id='.$_POST['id']);
	endif;
?>
